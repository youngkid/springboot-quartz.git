package org.demo.quartz.trigger.factory;

import org.demo.quartz.exception.TimingException;
import org.demo.quartz.mode.CronTimingModel;
import org.demo.quartz.mode.TriggerType;
import org.demo.quartz.mode.TimingModel;
import org.demo.quartz.trigger.ITriggerFactory;
import org.quartz.CronScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.stereotype.Component;

/**
 * @author Xiaohan.Yuan
 * @version 1.0.0
 * @ClassName CronTrigger.java
 * @Description
 * @createTime 2021年12月16日
 */
@Component
public class CronTrigger implements ITriggerFactory {
    @Override
    public boolean check(TriggerType triggerType) {
        return triggerType==TriggerType.CRON;
    }

    @Override
    public Trigger build(TimingModel timingModel) {
        if (!(timingModel instanceof CronTimingModel)){
            throw new TimingException("构建类型为CRON定时必须传入CronTimingModel.class的实现类");
        }
        //按新的cronExpression表达式构建一个新的trigger
        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(((CronTimingModel) timingModel).getCronExpression());

        TriggerBuilder<org.quartz.CronTrigger> cronTriggerTriggerBuilder = TriggerBuilder.newTrigger().withIdentity(timingModel.getTaskName(), timingModel.getTaskName())
                .withSchedule(scheduleBuilder);

        if (timingModel.getStartTime()!=null){
            cronTriggerTriggerBuilder.startAt(timingModel.getStartTime());
        }
        if (timingModel.getEndTime()!=null){
            cronTriggerTriggerBuilder.endAt(timingModel.getEndTime());
        }
        return cronTriggerTriggerBuilder.build();
    }
}
