package org.demo.quartz.log;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.quartz.JobExecutionContext;
import org.quartz.SimpleTrigger;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * 执行时触发日志
 * @author Xiaohan.Yuan
 * @date 2022/12/21
 */
@Aspect
@Component
@Slf4j
public class TaskJobLog {

    @Pointcut("execution( * org.demo.quartz.task.*.*(..))")
    public void taskJobLog(){

    }

    @Before(value = "taskJobLog()")
    public void beforeAdvice(JoinPoint joinPoint) {
        if(joinPoint.getArgs()==null||joinPoint.getArgs().length>1){
            return;
        }
        for (Object arg : joinPoint.getArgs()) {
            if (!(arg instanceof JobExecutionContext)){
                return;
            }
        }
        JobExecutionContext context =  (JobExecutionContext)joinPoint.getArgs()[0];
        // 获取任务名
        String name = context.getJobDetail().getJobBuilder().build().getKey().getName();
        // 获取任务分组
        String group = context.getJobDetail().getJobBuilder().build().getKey().getGroup();
        // 获取任务描述
        String description = context.getJobDetail().getDescription();

        Integer number = null;
        if (context.getTrigger() instanceof SimpleTrigger){
            // 运行次数
            number  = ((SimpleTrigger)context.getTrigger()).getTimesTriggered();
            // 开始时间
            Date startTime = context.getTrigger().getStartTime();
            // 结束时间
            Date endTime = context.getTrigger().getEndTime();

            // 获取参数
            String paramKey = context.getMergedJobDataMap().getString("paramKey");
            log.info("\n----------------------\n" +
                            "任务组:{}\n" +
                            "任务名:{}\n" +
                            "任务描述:{}\n" +
                            "运行次数:{}\n"+
                    "开始时间:{}\n"+
                    "结束时间:{}\n"+
                    "参数1:{}\n"+
                            "----------------------"
                    ,name,group,description,number,
                    startTime,
                    endTime,
                    paramKey
            );
        }


    }
}
